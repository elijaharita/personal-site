export default {
    css: ["~/assets/styles/normalize.css", "~/assets/styles/main.css"],
    head: {
        titleTemplate: s => s ? s + " - eseed.dev" : "eseed.dev",
        meta: [
            { charset: "utf-8" },
            { name: "viewport", content: "width=device-width, initial-scale=1" }
        ],
        script: [
            { type: "module", src: "https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js" }
        ]
    },
    components: true
}